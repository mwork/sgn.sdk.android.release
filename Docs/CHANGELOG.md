RC4
-------

This release brings push notification and some bug fixes. Please review **5.1.
Add permissions** and **5.4. Config for SGN** in Getting Started guide for
instruction to integrate push notification to your app.

RC3
-------

RC3 is released with the addition of item purchase API. To purchase an item,
call LoginWebView.setPaymentAccountVIP with the ID of the item the user wants
to buy. The callback is still the same, but PaymentInfo.itemID will be set with
the value of the item ID passed in setPaymentAccountVIP.

**Note**: the game has to check whether PaymentInfo.amount is enough to
actually purchase the item and acts accordingly. For example, if the amount is
smaller, convert the amount to in-game currency. If the amount is large, give
the item and convert the change to in-game currency.

RC2
-------

This release attempts at improving naming consistency as well as API clarity. Most notably:

- The SDK is packaged as a library project (like how Google Play Service
  library is packaged) so that game client could reuse the SDK resources (suc
  as strings).

- The unification of `GuestUser` and `UserInformation` into `User`: we provide a
  single class that encapsulates all data about the user.

- The addition of `StatusCode` which expresses various error conditions (such as
  `NO_NETWORK`, `SHARE_FAILED`, etc.) to the listeners.

- The addition of `PaymentInfo` which encapsulates all information about a
  payment (such as transaction ID, amount and more to come)

- Various changes in naming and method signatures, as well as deprecations of
  out-dated methods, to facilitates the above improvements. Thankfully, the
  _semantics_ of the methods don't change, so you can just update the current
  code base with new method names and signatures without worrying about
  impacting its correctness.

MOVE:

- `AutoLoginManager` (change package from `vn.mwork.sdk.googleaccount` to `vn.mwork.sdk.utils`)

REMOVE:

- `GuestListener` in favour of `AutoLoginListener`

- `GuestUser` and `UserInformation` in favour of `User`

ADD:

- `PaymentInfo` which encapsulates information about a payment

- `User` which encapsulates information about an User (both guest and linked)

DEPRECATED:

- `FacebookListener.onGetFriendListSuccess`; use `onGetFriendListSucceed`

- `FacebookListener.onGetFriendListFailed`; use `onFail(StatusCode)`

- `PaymentListener.onCardPaymentSuccess`; use `onCardPaymentFinish`

- `PaymentListener.onSendSMSPayment`; use `onSendSMSSucceed`

- `PaymentListener.onSMSPaymentSuccess`; use `onSMSPaymentSucceed`

- `PaymentListener.onFailed`; use `onFail`

- `LinkListener.onLinkAccountSuccess`; use `onLinkAccountSucceed`

- `LinkListener.onUnLinkAccount(String)`; use `onUnLinkAccount(User)`

- `LinkListener.onFailed`; use `onFail(StatusCode)`

- `AutoLoginListener.onSuccessGuestLogin`; use `onSucceed`

- `AutoLoginListener.onFailedGuestLogin`; use `onFail`
