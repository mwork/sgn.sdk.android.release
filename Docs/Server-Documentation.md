Server documentation
====================

The server endpoint for development is `123.30.210.138:5000`

User authentication
-------------------
![Authentication sequence][1]

 1. Game client calls authentication API from the SDK
 2. SDK returns user information, including user ID and their access
    token
    - User is logged in as guest by default, so game client can still
    retrieve user ID and access token even if they're not authenticated
    yet
 3. Game client could send the ID together with the token to game server for inspection in later steps
 4. The game server issues an access token inspection request to SDK server
 5. SDK server returns access token data, including user id
 6. Game server compares user ID returned by SDK server and the one sent by game client
 7. Game server confirms with the game client that the user ID/access token combination is valid
 
To verify access token, issue the following request:

 - `GET /debug_token?access_token=<token>` where `token` is SDK-issued access token
 - If the token is valid, status code 200 will be returned together with some information of the token, including user ID and client ID
 - If the token is valid, status code is 404


Payment
-------

![Payment sequence][2]

1. The game calls payment API from the SDK
2. The SDK returns transaction id, status and amount (if available)
3. The game sends transaction data to game server awaiting confirmation
4. SDK server calls a pre-registered endpoint in the game server to notify about transaction status when it completes
    - Note that there is no guarantee about the order of (3) and (4) (i.e. (4) may happen before (3), especially in the case of SMS payment)
5. Game server acknowledges SDK server's call by responding with HTTP status code 200. Otherwise, we will retries 5 times.
6. Game server validates the transaction based on the information it has
7. Game server confirms/notifies game client about the transaction

The game server must pre-registered an HTTP endpoint with SDK provider. When a transaction is completed, SDK server will send a POST request to the endpoint as follows:

- Content-Type: `application/json`
- Parameters:
    - `transaction_id`: the ID of the transaction
    - `timestamp`: UNIX timestamp of when the transaction occurs
    - `user_id`: the user who initiates the transaction
    - `game_id`: the game which initiates the transaction
    - `amount`: the amount in VND
    - `items`: a list of items the user purchased, which contain the following field
        - `item_id`: ID of the game item (if `game_item`)
        - **Note**:`items` is absent if the user just exchange money to in-game currency

Example
```javascript
{
    "transaction_id": "1e8e8ce7-9afe-474a-8976-b1b9099e9df7",
    "timestamp": 1401190562,
    "user_id": "123456789",
    "game_id": "1234",
    "amount": 100000,
    "items": [
        {
            "item_id": "321654987",
        }
    ]
}
```

  [1]: http://www.websequencediagrams.com/cgi-bin/cdraw?lz=dGl0bGUgQXV0aGVudGljYXRpb24gc2VxdWVuY2UKCkdhbWUtPlNESzoAFBByZXF1ZXN0ICgxKQpTREstPkdhbWU6IFVzZXIgaWQsIGFjY2VzcyB0b2tlbiAoMikARAdHYW1lIHNlcnZlcjogc2VuZCB1ABwWMwAsBgAlBwB9BwBIDWluc3BlYwCAfw40AIECCwBeCQB-DWRhdGEgKDUATw8AgQoNY29tcGFyZQCBNg0sAIEgCCAoNgApEzogY29uZmlybSAoNykKCg&s=rose
  [2]: http://www.websequencediagrams.com/cgi-bin/cdraw?lz=dGl0bGUgUGF5bWVudCBzZXF1ZW5jZQoKR2FtZS0-U0RLOgAUCXJlcXVlc3QgKDEpClNESy0-R2FtZTogVHJhbnNhY3Rpb24gaWQsIHN0YXR1cywgYW1vdW50ICgyKQBGB0dhbWUgc2VydmVyOiBzZW5kIHQAMAtkYXRhIGZvciBpbnNwZQBJBigzAF8LADIJACoMaWQAYQgsIHVzZXJfaWQgKDQAbAYAZQcAgT8HYWNrIChIVFRQAIEaByAyMDApICg1AB0PAIEYDWNvbmZpcm0gKDYAExMAFAs3KQ&s=rose