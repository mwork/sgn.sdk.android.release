SGN Android SDK Integration Guide
=====================

1. Import Facebook SDK
-----------------------
- In the Eclipse IDE: click File/Import --> show window --> chose Existing Android Code Into Workspace.

    ![][1]

- Browse in the folder contains facebook sdk

    ![][2]

    ![][3]

- Click Finish--> Result in the Eclipse IDE.

    ![][4]

2. Import Google Play service
--------------------------------------
- In the Eclipse IDE: click File/Import --> show window --> chose Existing Android Code Into Workspace.

    ![][5]
- Browse in the folder contains google-play-services_lib

    ![][6]

    ![][7]

- Click Finish--> Result in the Eclipse IDE.

    ![][8]

3. Import SGN SDK
-----------------
Same as importing Facebook SDK and Google Play service. The SDK project is inside SGNSDK/ directory.

4. Add Libs
----------------
- Facebook SDK:
    - Right click on your project, chose properties. At Android item click add and chose the facebook sdk

        ![][9]

        ![][10]

    - Result:

        ![][11]

- Google Play Service: Same integration as the Facebook SDK.

- SGN SDK: Same integration as the Facebook SDK.

5. Config in the AndroidManifest.xml
----------------
### 5.1. Add permissions ###
```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.GET_ACCOUNTS" />
<uses-permission android:name="android.permission.SEND_SMS" />
<uses-permission android:name="android.permission.USE_CREDENTIALS" />
<uses-permission android:name="android.permission.MANAGE_ACCOUNTS" />
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.WAKE_LOCK" />
<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
<permission
        android:name="vn.kvtm.khuvuontrenmay.permission.C2D_MESSAGE"
        android:protectionLevel="signature" />
<uses-permission android:name="vn.kvtm.khuvuontrenmay.permission.C2D_MESSAGE" />
<uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />
```

**Note**: `android:name` is app's packagename
```xml
<permission
    android:name="vn.kvtm.khuvuontrenmay.permission.C2D_MESSAGE"
    android:protectionLevel="signature" />
<uses-permission android:name="vn.kvtm.khuvuontrenmay.permission.C2D_MESSAGE" />
```

### 5.2. Config for Google Play Services ###
```xml
<meta-data
    android:name="com.google.android.gms.version"
    android:value="@integer/google_play_services_version" />
```

### 5.3. Config for Facebook SDK ###
```xml
<activity
    android:name="com.facebook.LoginActivity"
    android:label="@string/app_name"
    android:screenOrientation="landscape" />
<meta-data
    android:name="com.facebook.sdk.ApplicationId"
    android:value="@string/app_id" />
```

### 5.4. Config for SGN ###
- `strings.xml`
```xml
<string name="client_id">2371304797</string>
<string name="redirect_uri">http://panda3.gamevil.vn/oauth</string>
```
- `arrays.xml`
```xml
<string-array name="facebook_scopes"></string-array>
<string-array name="google_scopes"></string-array>
```
- `AndroidManifest.xml`
```xml
<meta-data
    android:name="vn.mwork.sdk.client_id_key"
    android:value="@string/client_id" />
<meta-data
    android:name="vn.mwork.sdk.redirect_uri_key"
    android:value="@string/redirect_uri" />
<meta-data
    android:name="vn.mwork.sdk.facebook_scopes"
    android:value="@array/facebook_scopes" />
<meta-data
    android:name="vn.mwork.sdk.google_scopes"
    android:value="@array/google_scopes" />
<meta-data
    android:name="vn.mwork.sdk.Environment"
    android:value="Test" />
    
<activity
    android:name="vn.msdk.AdActivity"
    android:configChanges="orientation|screenSize|keyboardHidden"
    android:screenOrientation="landscape"
    android:theme="@android:style/Theme.Translucent" />
    
<receiver
    android:name="com.google.android.mgcm.GCMBroadcastReceiver"
    android:permission="com.google.android.c2dm.permission.SEND" >
    <intent-filter>
        <action android:name="com.google.android.c2dm.intent.RECEIVE" />
        <action android:name="com.google.android.c2dm.intent.REGISTRATION" />
        <action android:name="android.intent.action.PACKAGE_REMOVED" />
        
        <category android:name="vn.kvtm.khuvuontrenmay" />
        </intent-filter>
</receiver>
<service android:name="vn.msdk.GCMIntentService" />
<service android:name="vn.msdk.appboost.service.AdService" />
<service android:name="vn.msdk.appboost.service.PrivateAdService" />
<receiver android:name="vn.msdk.appboost.receiver.AlarmReceiver" />
```

**Note**: `android:name` is app's packagename
```xml
<category android:name="vn.kvtm.khuvuontrenmay" />
```

### 5.5. Example ###
```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="vn.kvtm.khuvuontrenmay"
    android:versionCode="1"
    android:versionName="1.0" >

    <uses-sdk
        android:minSdkVersion="9"
        android:targetSdkVersion="19" />

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.GET_ACCOUNTS" />
    <uses-permission android:name="android.permission.SEND_SMS" />
    <uses-permission android:name="android.permission.USE_CREDENTIALS" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.WAKE_LOCK" />
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />

    <permission
        android:name="vn.kvtm.khuvuontrenmay.permission.C2D_MESSAGE"
        android:protectionLevel="signature" />

    <uses-permission android:name="vn.kvtm.khuvuontrenmay.permission.C2D_MESSAGE" />
    <uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />

    <application
        android:allowBackup="true"
        android:icon="@drawable/ic_launcher"
        android:label="@string/app_name"
        android:theme="@style/AppTheme" >
        <activity
            android:name="vn.kvtm.khuvuontrenmay.LoadingActivity"
            android:configChanges="orientation|screenSize|keyboardHidden"
            android:label="@string/app_name"
            android:screenOrientation="landscape"
            android:theme="@android:style/Theme.Black.NoTitleBar.Fullscreen" >
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
        <activity
            android:name="vn.kvtm.khuvuontrenmay.LoginActivity"
            android:hardwareAccelerated="false"
            android:screenOrientation="landscape"
            android:theme="@android:style/Theme.Black.NoTitleBar.Fullscreen"
            android:windowSoftInputMode="adjustResize|adjustPan|adjustUnspecified" />
        <activity
            android:name="vn.msdk.AdActivity"
            android:configChanges="orientation|screenSize|keyboardHidden"
            android:screenOrientation="landscape"
            android:theme="@android:style/Theme.Translucent" />

        <receiver
            android:name="com.google.android.mgcm.GCMBroadcastReceiver"
            android:permission="com.google.android.c2dm.permission.SEND" >
            <intent-filter>
                <action android:name="com.google.android.c2dm.intent.RECEIVE" />
                <action android:name="com.google.android.c2dm.intent.REGISTRATION" />
                <action android:name="android.intent.action.PACKAGE_REMOVED" />

                <category android:name="vn.kvtm.khuvuontrenmay" />
            </intent-filter>
        </receiver>

        <service android:name="vn.msdk.GCMIntentService" />
        <service android:name="vn.msdk.appboost.service.AdService" />
        <service android:name="vn.msdk.appboost.service.PrivateAdService" />

        <receiver android:name="vn.msdk.appboost.receiver.AlarmReceiver" />

        <meta-data
            android:name="com.google.android.gms.version"
            android:value="@integer/google_play_services_version" />

        <activity
            android:name="com.facebook.LoginActivity"
            android:label="@string/app_name"
            android:screenOrientation="landscape" />

        <meta-data
            android:name="com.facebook.sdk.ApplicationId"
            android:value="@string/app_id" />
        <meta-data
            android:name="vn.mwork.sdk.Environment"
            android:value="@string/vn.mwork.sdk.Environment" />
        <meta-data
            android:name="vn.mwork.sdk.redirect_uri_key"
            android:value="@string/redirect_uri" />
        <meta-data
            android:name="vn.mwork.sdk.client_id_key"
            android:value="@string/client_id" />
        <meta-data
            android:name="vn.mwork.sdk.facebook_scopes"
            android:value="@array/facebook_scopes" />
        <meta-data
            android:name="vn.mwork.sdk.google_scopes"
            android:value="@array/google_scopes" />
    </application>

</manifest>
```

6. Initialize the SDK
---------------------
- To initialize the SDK, call `SDKUtility.getInstance`. User information is only
available after the game initialize the SDK. The initialization will check
whether the user is already logged in and, if not, log the user into a guest
account.
```java
/**
 * Initialize the SDK
 * 
 * @param activity
 * @param autoLoginListener
 * @return 
 */
public static SDKUtility getInstance(Activity activity, AutoLoginListener autoLoginListener);
```

- `SDKUtility.getInstance` requires an `AutoLoginListener`, which needs to implement the
following interface
```java
/**
 * Callback interface for auto-login.
 */
public interface AutoLoginListener {

        /**
         * Callback when the user is auto-logged-in successfully
         * 
         * @param user
         *            the user that's is auto-logged-in
         */
        public void onSucceed(User user);

        /**
         * Callback when auto-login failed.
         */
        public void onFail();
}
```

7. Add function Link
--------------------------
- The following performs Link current account with the Facebook/Google Play/SGN account. Call `setLinkAccount()` method in the `LoginWebView` class to show account linking page.
```java
    /**
    * Set data when link with facebook, google, sgn account
    *
    * @param activity: Activity that show webview
    * @param bundle: bundle of the activity that show webview
    * @param listener: LinkListener to handle linking result
    */
    public void setLinkAccount(Activity activity, Bundle bundle, LinkListener listener);
```

- Implement `LinkListener` interface to handle event for the Link Account and Profile functions.
```java
/**
 * Callback when getting Facebook token successfully.
 */
void onGetTokenFacebook(String token, String username);

/**
 * Callback when getting Google token successfully
 */
void onGetTokenGooglePlay(String token, String username);

/**
 * Callback when user finishes account linking successfully.
 */
public void onLinkAccountSucceed(boolean userSwitched, User user);

/**
 * Callback when the user unlinks their account.
 */
public void onUnLinkAccount(User guestUser);

/**
 * Callback when there's error while linking the account.
 */
public void onFail(StatusCode code);
```

- `StatusCode` is an enum of SDK status codes, which are used in various listeners' `onFail` callback
```java
public static enum StatusCode {
    NO_NETWORK,
    ERROR_NETWORK,
    NO_GOOGLE_ACCOUNT,
    NOT_LOGIN_FACEBOOK,
    SHARE_FAILED,
    CANCEL_INVITE,
    INVITE_FAILED
}
```

8. Add function Profile
-----------------------------
Call `setProfileAccount()` method in the `LoginWebView` class to show profile information of user.
```java
/**
 * @param activity: is an Activity that shows webview.
 * @param bundle: is bundle of the activity that shows webview.
 */
public void setProfileAccount(Activity activity, Bundle bundle);
```

9. Add function Invite
----------------------------
The function allows user invite their friends to play a game. To invite friends, first implement `FacebookListener`, then invoke `LoginFacebook.getListFriends` with the listener. The method `LoginFacebook.sendRequestDialog` should be called inside the listener's `onGetFriendListSucceed` callback.

- The `FacebookListener` interface to handle getting friend list from Facebook
```java
/**
 * Callback when getting FB friend list successfully.
 */
public void onGetFriendListSucceed(ArrayList<String> ids,
                HashMap<String, String> friends);

/**
 * Callback when communication with the facebook failed.
 */
public void onFail(StatusCode code);

/**
 * Callback method when invite Facebook friends successfully.
 */
public void onInviteFacebookSucceed();
```

- Relevant `LoginFacebook` methods
```java
/**
    * @param session: Facebook session. It should be already opened.
    * @param listener: listener to handle result
    */
public void getListFriends(Session session, final FacebookListener listener);

/**
    * @param list_friend_id: friend IDs, separated by a comma
    * @param context
    * @param message: the message in the invitation request
    * @param listener: listener to handle result
    */
public void sendRequestDialog(String list_friend_id, final Context context, String message,
                                final FacebookListener listener);
```

10. Add function Share
---------------------------
To share a picture to user's Facebook profile, call `LoginFacebook.postPhotoWallFacebook`

```java
/**
 * Post photo to user's wall
 *
 * @param bm: the picture as a Bitmap
 * @param msg: the message that appear with the picture
 * @param listener: listener to handle result
 */
public void postPhotoWallFacebook(Bitmap bm, String msg,
                                    final FacebookListener listener);
```
- The `FacebookListener` interface to handle posting result from Facebook
```java
/**
 * Callback method when share photos on Facebook successfully.
 */
public void onShareFacebookSucceed();
```

11. Add function Payment
-----------------------------
The function show the payment page, allow user payment. Use the `PaymentListener` interface to listen event about payment.

The user could pay via telephone card or SMS. To handle SMS payment, call `PaymentUtils.setupPayment` with a `PaymentListener`. To handle card payment and display the payment dialog, call `LoginWebView.setPaymentAccount` to purchase in-game currency or `LoginWebView.setPaymentAccountVIP` to purchase in-game items.

- `PaymentUtils` methods
```java
/**
 * Set up payment, polling periodically to get information about
 * payment
 * 
 * @param context
 * @param paymentListener
 */
public void setupPayment(Context context,
                final PaymentListener paymentListener) {
/**
 * Cancel payment information polling
 * 
 * @param context
 */
public void unSetupPayment(Context context);
```

- `LoginWebView` methods
```java
/**
* @param activity
* @param bundle
* @param listener: listener to handle card payment result
*/
public void setPaymentAccount(Activity activity, Bundle bundle, PaymentListener listener);

/**
 * Set data when user click payment vip
 * 
 * @param activity
 * @param bundle
 * @param listener: listener to handle card payment result
 * @param itemtID: id of an item to purchase
 * 
 * @return payment view
 */
public void setPaymentAccountVIP(Activity activity, Bundle bundle,
    PaymentListener listener, String itemtID);
```

### 11.1 Payment via cards ###
The `PaymentListener` interface provide methods to listen to successful/failed card payments.

```java
/**
 * callback method when card payment finished
 *
 * @param isSuccess: whether payment succeeds or fails
 * @param payment: information about the payment.
 */
public void onCardPaymentFinish(boolean isSuccess, PaymentInfo payment);

/**
 * Callback when there's an error while user makes a payment.
 */
public void onFail();
```
- `PaymentInfo`
```java
public class PaymentInfo {
    private String transID; /* transaction id of the payment transaction */
    private long amount; /* money that user have charged */
    private String itemID = ""; /* id of item. */

    public String getTransID() {
        return transID;
    }

    public long getAmount() {
        return amount;
    }

    public String getItemID() {
        return itemID;
    }
}
```


### 11.2 Payment via SMS ###
The `PaymentListener` interface provide methods to listen to successful SMS payments.

```java
/**
 * Callback invoked when the user sends the SMS message
 *
 * @param number
 * @param message
 */
public void onSendSMSSucceed(String number, String message);

/**
 * Callback invoked the SMS payment is processed successfully on the server
 *
 * @param payment: information about the payment
 */
public void onSMSPaymentSucceed(PaymentInfo payment);
```

12. Get user information
------------------------------
Call helper methods of `User` to retrieve user information such as user ID, user name and access tokens
```java
// Get user ID
public static String getUserId(Context context);

// Get access token
public static String getTokenServerReturn(Context context);

// Get Facebook token
public static String getFacebookToken(Context context);

// Get Facebook name
public static String getFacebookName(Context context);

// Get Google token
public static String getGoogleToken(Context context);

// Get Google name
public static String getGoogleName(Context context);
```

  [1]: http://i.imgur.com/DJqH3NQ.png
  [2]: http://i.imgur.com/aQTDkLk.png
  [3]: http://i.imgur.com/g1QRdrW.png
  [4]: http://i.imgur.com/kGtbutb.png
  [5]: http://i.imgur.com/DJqH3NQ.png
  [6]: http://i.imgur.com/aQTDkLk.png
  [7]: http://i.imgur.com/pav28OX.png
  [8]: http://i.imgur.com/r0AHj96.png
  [9]: http://i.imgur.com/rePX5a5.png
  [10]: http://i.imgur.com/ewwRk0R.png
  [11]: http://i.imgur.com/9846UPe.png
  [12]: http://i.imgur.com/REDsehz.png
