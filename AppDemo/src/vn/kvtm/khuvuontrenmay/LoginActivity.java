package vn.kvtm.khuvuontrenmay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import vn.kvtm.khuvuontrenmay.utility.ImageUtility;
import vn.mwork.sdk.facebook.LoginFaceBook;
import vn.mwork.sdk.googleaccount.AbstractGetNameTask;
import vn.mwork.sdk.googleaccount.GoogleAccount;
import vn.mwork.sdk.interfaces.FacebookListener;
import vn.mwork.sdk.interfaces.LinkListener;
import vn.mwork.sdk.interfaces.PaymentListener;
import vn.mwork.sdk.payments.PaymentUtils;
import vn.mwork.sdk.utils.PaymentInfo;
import vn.mwork.sdk.utils.User;
import vn.mwork.sdk.utils.Webconfig.StatusCode;
import vn.mwork.sdk.utils.WelcomeForm;
import vn.mwork.sdk.widget.LoginWebView;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class LoginActivity extends Activity implements OnClickListener {

	private static ArrayList<String> INVITE_ARRAY;

	private ImageView linkImage;
	private ImageView paymentImage;
	private ImageView postImage;
	private ImageView inviteImage;
	private ImageView contactImage;
	private ImageView paymentVIP1Image;
	private ImageView paymentVIP2Image;
	private Bundle mBundle;
	private LoginFaceBook mLoginFace;
	private GoogleAccount googleAccount;
	private Dialog mDialog;
	private LoginWebView mLoginWebView;

	private ImageLoader imageLoader;
	private DisplayImageOptions options;

	private LinearLayout welcomeLayout;
	private TextView welcomeText;

	private Context context;

	private ArrayList<String> idListChecked;
	private ArrayList<String> nameListChecked;
	private ArrayList<String> avatarListChecked;

	private ProgressBar progressBar;

	private ImageView imageFriend1;
	private ImageView imageFriend2;
	private ImageView imageFriend3;

	private TextView textFriend1;
	private TextView textFriend2;
	private TextView textFriend3;

	private LinearLayout infoLayout;
	private Button inviteButton;
	private ProgressBar linkProgress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		context = this.getApplicationContext();
		mBundle = savedInstanceState;

		INVITE_ARRAY = new ArrayList<String>();
		INVITE_ARRAY.add("703943502");
		INVITE_ARRAY.add("100002453838614");
		INVITE_ARRAY.add("100001503518350");
		INVITE_ARRAY.add("100000435270590");
		INVITE_ARRAY.add("100000008597647");
		INVITE_ARRAY.add("1700692335");
		INVITE_ARRAY.add("799069306");
		INVITE_ARRAY.add("100004140251670");
		INVITE_ARRAY.add("100000102346932");
		INVITE_ARRAY.add("1045309746");
		INVITE_ARRAY.add("1443277783");

		this.imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration
				.createDefault(LoginActivity.this));
		this.options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true)
				.cacheOnDisc(true).build();

		googleAccount = new GoogleAccount(this);
		mLoginFace = new LoginFaceBook(this, savedInstanceState);
		setContentView(R.layout.login_successfully);

		welcomeLayout = (LinearLayout) findViewById(R.id.welcome_layout);
		welcomeLayout.getBackground().setAlpha(180);
		welcomeText = (TextView) findViewById(R.id.welcome_text);

		String accountName = WelcomeForm.getAccountName(context);
		WelcomeForm.setWelcomeForm(context, accountName, AnimationUtils
				.loadAnimation(context, R.anim.welcome_form_animation),
				welcomeLayout, welcomeText);

		linkImage = (ImageView) findViewById(R.id.link_image);
		linkImage.setOnClickListener(this);

		paymentImage = (ImageView) findViewById(R.id.payment_image);
		paymentImage.setOnClickListener(this);

		paymentVIP1Image = (ImageView) findViewById(R.id.payment_vip1_image);
		paymentVIP1Image.setOnClickListener(this);

		paymentVIP2Image = (ImageView) findViewById(R.id.payment_vip2_image);
		paymentVIP2Image.setOnClickListener(this);

		postImage = (ImageView) findViewById(R.id.post_image);
		postImage.setOnClickListener(this);
		inviteImage = (ImageView) findViewById(R.id.invite_image);
		inviteImage.setOnClickListener(this);
		contactImage = (ImageView) findViewById(R.id.contact_image);
		contactImage.setOnClickListener(this);

		PaymentUtils.getInstance().setupPayment(context, paymentListener);

	}

	@Override
	protected void onResume() {
		super.onResume();

		if (mLoginWebView != null) {
			mLoginWebView.onResumeHelper();
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (mLoginWebView != null) {
			mLoginWebView.onDestroyHelper();
		}
		PaymentUtils.getInstance().unSetupPayment(context);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (mLoginWebView != null) {
			mLoginWebView.onActivityResultHelper(requestCode, resultCode, data);
		}

		if (requestCode == AbstractGetNameTask.mRequestCode) {
			if (resultCode == RESULT_OK) {
				googleAccount.syncGoogleAccount(context, listenerLink,
						mLoginWebView);
			}
		}
	}

	/**
	 * Show dialog confirm user when share
	 */
	public void showAlertShare() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set title
		alertDialogBuilder.setTitle(R.string.confirm_title_dialog);

		// set dialog message
		alertDialogBuilder
				.setMessage(R.string.confirm_content_dialog)
				.setCancelable(false)
				.setPositiveButton(android.R.string.yes,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								BitmapDrawable dr = (BitmapDrawable) (getResources()
										.getDrawable(R.drawable.game_screen));
								Bitmap bm = dr.getBitmap();
								mLoginFace.postPhotoWallFacebook(bm,
										"Great game! http://kvtm.vn",
										facebookListener);
							}
						})
				.setNegativeButton(android.R.string.no,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	@Override
	public void onClick(View v) {
		Session session = Session.getActiveSession();
		String accountName = User.getFacebookName(context);
		switch (v.getId()) {
		case R.id.link_image:
			showDialogLink();
			break;

		case R.id.payment_image:
			showDialogPayment();

			break;
		case R.id.payment_vip1_image:
			showDialogPaymentVIP("1");

			break;
		case R.id.payment_vip2_image:
			showDialogPaymentVIP("2");

			break;

		case R.id.post_image:
			if (TextUtils.isEmpty(accountName)) {
				Toast.makeText(getApplicationContext(),
						R.string.not_facebook_login, Toast.LENGTH_SHORT).show();
				return;
			}
			showAlertShare();
			break;

		case R.id.invite_image:
			if (TextUtils.isEmpty(accountName)) {
				Toast.makeText(getApplicationContext(),
						R.string.not_facebook_login, Toast.LENGTH_SHORT).show();
				return;
			}

			mLoginFace.getListFriends(session, facebookListener);
			showFriendListDialog();

			break;

		case R.id.contact_image:
			showProfileDialog();
			break;

		default:
			break;
		}
	}

	/**
	 * Show dialog content webview for link facebook, google, SGN
	 */
	public void showDialogLink() {
		mDialog = new Dialog(this, R.style.NewDialog);

		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View popupView = layoutInflater.inflate(R.layout.loginwebview, null);
		mLoginWebView = (LoginWebView) popupView
				.findViewById(R.id.loginwebview);

		linkProgress = (ProgressBar) popupView.findViewById(R.id.progressbar);
		ImageView closeView = (ImageView) popupView
				.findViewById(R.id.close_icon);

		closeView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
				mDialog = null;
			}
		});
		mLoginWebView.setLinkAccount(this, mBundle, listenerLink);
		mLoginWebView.setWebChromeClient(new WebChromeClient() {

			public void onProgressChanged(WebView view, int pro) {
				if (pro > 90) {
					mLoginWebView.setVisibility(View.VISIBLE);
					linkProgress.setVisibility(View.GONE);
				}

			}
		});

		mDialog.addContentView(popupView, new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		if (!this.isFinishing()) {
			mDialog.show();
		}

	}

	/**
	 * Show profile dialog
	 */
	private void showProfileDialog() {
		mDialog = new Dialog(LoginActivity.this, R.style.NewDialog);
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View popupView = layoutInflater.inflate(R.layout.loginwebview, null);
		mLoginWebView = (LoginWebView) popupView
				.findViewById(R.id.loginwebview);
		ImageView closeView = (ImageView) popupView
				.findViewById(R.id.close_icon);

		linkProgress = (ProgressBar) popupView.findViewById(R.id.progressbar);
		closeView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});

		mLoginWebView.setProfileAccount(this, mBundle);
		mLoginWebView.setWebChromeClient(new WebChromeClient() {

			public void onProgressChanged(WebView view, int pro) {
				if (pro > 90) {
					mLoginWebView.setVisibility(View.VISIBLE);
					linkProgress.setVisibility(View.GONE);
				}
			}
		});

		mDialog.addContentView(popupView, new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		if (!this.isFinishing()) {
			mDialog.show();
		}

	}

	/**
	 * show dialog payment
	 */
	public void showDialogPayment() {
		mDialog = new Dialog(LoginActivity.this, R.style.NewDialog);
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View popupView = layoutInflater.inflate(R.layout.payment_form, null);
		mLoginWebView = (LoginWebView) popupView
				.findViewById(R.id.payment_view);

		ImageView closeView = (ImageView) popupView
				.findViewById(R.id.close_icon);

		final ProgressBar progress = (ProgressBar) popupView
				.findViewById(R.id.progressbar_payment);
		closeView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});

		mLoginWebView.setPaymentAccount(this, mBundle, paymentListener);
		mLoginWebView.requestFocus();
		mLoginWebView.setWebChromeClient(new WebChromeClient() {

			public void onProgressChanged(WebView view, int pro) {
				if (pro > 80) {
					mLoginWebView.setVisibility(View.VISIBLE);
					progress.setVisibility(View.GONE);
				}
			}

		});

		mDialog.addContentView(popupView, new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		if (!this.isFinishing()) {
			mDialog.show();
		}

	}

	/**
	 * show dialog payment
	 */
	public void showDialogPaymentVIP(String itemID) {
		mDialog = new Dialog(LoginActivity.this, R.style.NewDialog);
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View popupView = layoutInflater.inflate(R.layout.payment_form, null);
		mLoginWebView = (LoginWebView) popupView
				.findViewById(R.id.payment_view);

		ImageView closeView = (ImageView) popupView
				.findViewById(R.id.close_icon);

		final ProgressBar progress = (ProgressBar) popupView
				.findViewById(R.id.progressbar_payment);
		closeView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});

		mLoginWebView.setPaymentAccountVIP(this, mBundle, paymentListener,
				itemID);
		mLoginWebView.requestFocus();
		mLoginWebView.setWebChromeClient(new WebChromeClient() {

			public void onProgressChanged(WebView view, int pro) {
				if (pro > 80) {
					mLoginWebView.setVisibility(View.VISIBLE);
					progress.setVisibility(View.GONE);
				}
			}

		});

		mDialog.addContentView(popupView, new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		if (!this.isFinishing()) {
			mDialog.show();
		}

	}

	/**
	 * Show AnimationForm
	 * 
	 * @param mount
	 */
	private void showAnimationForm(String mount, String itemID) {
		if (null != welcomeLayout) {
			welcomeLayout.setVisibility(View.VISIBLE);
			if (itemID.equals("")) {
				WelcomeForm.setPaymentForm(context, mount, AnimationUtils
						.loadAnimation(context, R.anim.welcome_form_animation),
						welcomeLayout, welcomeText);
			} else {
				WelcomeForm.setPaymentForm(context, mount, AnimationUtils
						.loadAnimation(context, R.anim.welcome_form_animation),
						welcomeLayout, welcomeText, itemID);
			}

		}
	}

	/**
	 * Dialog show 3 friends on facebook for invite
	 */
	private void showFriendListDialog() {
		idListChecked = new ArrayList<String>();
		nameListChecked = new ArrayList<String>();
		avatarListChecked = new ArrayList<String>();

		final Dialog dialog = new Dialog(LoginActivity.this, R.style.NewDialog);

		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View popupView = layoutInflater.inflate(R.layout.invite_form, null);

		imageFriend1 = (ImageView) popupView.findViewById(R.id.avatar_friend1);
		imageFriend2 = (ImageView) popupView.findViewById(R.id.avatar_friend2);
		imageFriend3 = (ImageView) popupView.findViewById(R.id.avatar_friend3);

		textFriend1 = (TextView) popupView.findViewById(R.id.name_friend1);
		textFriend2 = (TextView) popupView.findViewById(R.id.name_friend2);
		textFriend3 = (TextView) popupView.findViewById(R.id.name_friend3);

		infoLayout = (LinearLayout) popupView.findViewById(R.id.info_layout);
		progressBar = (ProgressBar) popupView.findViewById(R.id.progressbar);

		inviteButton = (Button) popupView.findViewById(R.id.invite_friend);
		inviteButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				StringBuilder idCheckedList = new StringBuilder();
				// int size = idListChecked.size();
				for (int i = 0; i < 3; i++) {
					idCheckedList.append(idListChecked.get(i));
					if (i < 2) {
						idCheckedList.append(",");
					}
				}

				mLoginFace.sendRequestDialog(idCheckedList.toString(),
						LoginActivity.this, "Game hay lắm. Vào chơi đi!",
						facebookListener);
				idListChecked.clear();
				nameListChecked.clear();
				avatarListChecked.clear();

				dialog.dismiss();
			}
		});

		ImageView closeImage = (ImageView) popupView
				.findViewById(R.id.close_icon);
		closeImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				idListChecked.clear();
				nameListChecked.clear();
				avatarListChecked.clear();

				dialog.dismiss();
			}
		});

		dialog.addContentView(popupView, new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		dialog.show();

	}

	/**
	 * Set avatar for invite dialog
	 * 
	 * @param path
	 * @param avatar
	 */
	private void setAvatarFriend(String path, final ImageView avatar) {
		imageLoader.displayImage(path, avatar, options,
				new SimpleImageLoadingListener() {
					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						Bitmap scaleBitmapImage;
						scaleBitmapImage = Bitmap.createScaledBitmap(
								loadedImage,
								(int) convertDpToPixel(54, context),
								(int) convertDpToPixel(54, context), false);

						Bitmap roundBitmap = ImageUtility.getCroppedBitmap(
								scaleBitmapImage,
								(int) convertDpToPixel(60, context));

						avatar.setImageBitmap(roundBitmap);
					}
				});
	}

	/**
	 * Get 3 shuffle facebook friends
	 * 
	 * @param friendsID
	 * @param friendName
	 */
	public void shuffleFacebook(ArrayList<String> friendsID,
			HashMap<String, String> friendName) {
		ArrayList<String> idList = friendsID;
		HashMap<String, String> nameList = friendName;
		idListChecked.clear();
		nameListChecked.clear();
		Random rnd = new Random();
		for (int i = 0; i < 3; i++) {
			int index = rnd.nextInt(idList.size() - 1);

			String id = idList.get(index);
			String name = nameList.get(id);
			idListChecked.add(id);
			nameListChecked.add(name);

			String pathIcon = "http://graph.facebook.com/" + id
					+ "/picture?type=large";
			avatarListChecked.add(pathIcon);

			idList.remove(index);
			nameList.remove(id);
		}
	}

	/**
	 * Get 3 friend facebook
	 */
	public void getThreeFriends() {
		int faceSize = INVITE_ARRAY.size();
		for (int i = 0; i < faceSize; i++) {
			String user = INVITE_ARRAY.get(i);
			if (LoginWebView.idFaceList.contains(user)) {
				idListChecked.add(user);
				nameListChecked.add(LoginWebView.nameFaceList.get(user));
				String pathIcon = "http://graph.facebook.com/" + user
						+ "/picture?type=large";
				avatarListChecked.add(pathIcon);
			}
		}
	}

	private float convertDpToPixel(float dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	private FacebookListener facebookListener = new FacebookListener() {
		@Override
		public void onGetFriendListSucceed(ArrayList<String> ids,
				HashMap<String, String> friends) {
			if (LoginWebView.idFaceList.size() == 0) {

				LoginWebView.idFaceList.addAll(ids);
				LoginWebView.nameFaceList.putAll(friends);
			}

			// getThreeFriends();
			shuffleFacebook(LoginWebView.idFaceList, LoginWebView.nameFaceList);

			progressBar.setVisibility(View.GONE);
			infoLayout.setVisibility(View.VISIBLE);
			inviteButton.setVisibility(View.VISIBLE);

			int sizeFriend = nameListChecked.size();
			if (sizeFriend > 0) {
				textFriend1.setText(nameListChecked.get(0));
				setAvatarFriend(avatarListChecked.get(0), imageFriend1);
			}

			if (sizeFriend > 1) {
				textFriend2.setText(nameListChecked.get(1));
				setAvatarFriend(avatarListChecked.get(1), imageFriend2);
			}

			if (sizeFriend > 2) {
				textFriend3.setText(nameListChecked.get(2));
				setAvatarFriend(avatarListChecked.get(2), imageFriend3);
			}

		}

		@Override
		public void onFail(StatusCode code) {
			if (code == StatusCode.NO_NETWORK) {

				Toast.makeText(LoginActivity.this, "Network not connected",
						Toast.LENGTH_SHORT).show();

			} else if (code == StatusCode.ERROR_NETWORK) {

				Toast.makeText(LoginActivity.this, "Network Error",
						Toast.LENGTH_SHORT).show();

			} else if (code == StatusCode.NOT_LOGIN_FACEBOOK) {

				Toast.makeText(LoginActivity.this, "Facebook is not logined",
						Toast.LENGTH_SHORT).show();

			} else if (code == StatusCode.CANCEL_INVITE) {

				Toast.makeText(LoginActivity.this, "Invite cancelled",
						Toast.LENGTH_SHORT).show();

			} else if (code == StatusCode.INVITE_FAILED) {

				Toast.makeText(LoginActivity.this, "Invite failed",
						Toast.LENGTH_SHORT).show();

			} else if (code == StatusCode.SHARE_FAILED) {

				Toast.makeText(LoginActivity.this, "Share failed",
						Toast.LENGTH_SHORT).show();

			}

		}

		@Override
		public void onShareFacebookSucceed() {
			Toast.makeText(LoginActivity.this, "Share successfully",
					Toast.LENGTH_SHORT).show();

		}

		@Override
		public void onInviteFacebookSucceed() {
			Toast.makeText(LoginActivity.this, "Invite successfully",
					Toast.LENGTH_SHORT).show();

		}
	};

	private LinkListener listenerLink = new LinkListener() {
		@Override
		public void onGetTokenGooglePlay(String googleToken, String user) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (null != linkProgress) {
						linkProgress.setVisibility(View.VISIBLE);
					}

				}
			});

		}

		@Override
		public void onGetTokenFacebook(String faceToken, String user) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (null != linkProgress) {
						linkProgress.setVisibility(View.VISIBLE);
					}

				}
			});
		}

		@Override
		public void onLinkAccountSucceed(boolean userSwitched, User user) {
			if (mDialog != null || mDialog.isShowing()) {
				mDialog.dismiss();
				mDialog = null;
			}

			Toast.makeText(LoginActivity.this, R.string.login_successfully,
					Toast.LENGTH_SHORT).show();

		}

		@Override
		public void onUnLinkAccount(User guestUser) {
			if (mDialog != null || mDialog.isShowing()) {
				mDialog.dismiss();
				mDialog = null;
				Toast.makeText(LoginActivity.this,
						R.string.unlink_successfully, Toast.LENGTH_SHORT)
						.show();

			}

		}

		@Override
		public void onFail(StatusCode code) {
			if (code == StatusCode.NO_NETWORK) {

			} else if (code == StatusCode.ERROR_NETWORK) {

			} else if (code == StatusCode.NO_GOOGLE_ACCOUNT) {

			}

		}
	};

	private PaymentListener paymentListener = new PaymentListener() {
		@Override
		public void onCardPaymentFinish(boolean isSuccess, PaymentInfo payment) {
			if (mDialog != null || mDialog.isShowing()) {
				mDialog.dismiss();
				mDialog = null;

				if (isSuccess) {
					showAnimationForm(String.valueOf(payment.getAmount()),
							payment.getItemID());
					Log.d("itemID",
							"onCardPaymentFinish,itemID: "
									+ payment.getItemID());
				}

			}

		}

		@Override
		public void onSMSPaymentSucceed(PaymentInfo payment) {
			if (payment.getAmount() > 0) {
				showAnimationForm(String.valueOf(payment.getAmount()),
						payment.getItemID());
			}

		}

		@Override
		public void onFail() {
			Toast.makeText(context, "Payment error!", Toast.LENGTH_LONG).show();
		}

		@Override
		public void onSendSMSSucceed(String numberPhone, String message) {
			if (mDialog != null || mDialog.isShowing()) {
				mDialog.dismiss();
				mDialog = null;
			}

		}
	};

}
