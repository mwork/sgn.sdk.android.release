package vn.kvtm.khuvuontrenmay;

import vn.mwork.sdk.interfaces.AutoLoginListener;
import vn.mwork.sdk.utils.SDKUtility;
import vn.mwork.sdk.utils.User;
import vn.mwork.sdk.utils.Webconfig;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * splash screen
 * 
 * @author huannv
 * 
 */
public class LoadingActivity extends Activity {

	public static final String USER_ID_LOGIN_SERVER_RETURN = "USER_ID_LOGIN_SERVER_RETURN";

	private ProgressBar progressBar;
	private TextView progressBarText;
	private TextView idTextView;

	private Handler progressBarHandler;

	private int progress = 0;
	private long fileSize = 0;
	private String userId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		progressBarHandler = new Handler();

		setContentView(R.layout.loading_activity);

		progressBarText = (TextView) findViewById(R.id.progressbar_text);
		progressBar = (ProgressBar) findViewById(R.id.progressbar);
		idTextView = (TextView) findViewById(R.id.id_textview);

		userId = User.getUserId(LoadingActivity.this);
		idTextView.setText(userId);

		updateProgressBar();

		SDKUtility.getInstance(this, listener);

	}

	/**
	 * Update text, percent for progressbar
	 */
	private void updateProgressBar() {
		fileSize = 0;
		new Thread(new Runnable() {
			public void run() {
				while (progress < 100) {
					// process some tasks
					progress = doSomeTasks();
					// your computer is too fast, sleep 1 second
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// Update the progress bar
					progressBarHandler.post(new Runnable() {
						public void run() {
							if (progressBar == null)
								return;
							progressBar.setProgress(progress);
							progressBarText.setText(progress + "%");
						}
					});
				}

				if (progress == 100) {
					// sleep 2 seconds, so that you can see the 100%
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (userId != null) {
						Intent intent = new Intent(LoadingActivity.this,
								LoginActivity.class);
						startActivity(intent);
						finish();
					} else {
						finish();
					}
					// close the progress bar dialog

				}
			}
		}).start();
	}

	private int doSomeTasks() {
		while (fileSize <= 1000000) {
			fileSize++;
			if (fileSize == 100000) {
				return 10;
			} else if (fileSize == 200000) {
				return 20;
			} else if (fileSize == 300000) {
				return 30;
			} else if (fileSize == 400000) {
				return 40;
			} else if (fileSize == 500000) {
				return 50;
			} else if (fileSize == 600000) {
				return 60;
			} else if (fileSize == 700000) {
				return 70;
			} else if (fileSize == 800000) {
				return 80;
			} else if (fileSize == 900000) {
				return 90;
			}
		}

		return 100;

	}

	/**
	 * Listener event when get userID successfully
	 */
	private AutoLoginListener listener = new AutoLoginListener() {

		@Override
		public void onSucceed(User user) {
			if (TextUtils.isEmpty(userId)) {
				userId = user.getUserId();
				idTextView.setText(userId);
			}

		}

		@Override
		public void onFail() {
			Toast.makeText(LoadingActivity.this, "Login error",
					Toast.LENGTH_SHORT).show();

		}
	};

}
